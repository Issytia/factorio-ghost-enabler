function enable_ghosts_force(force)
	force.technologies["issy-enable-ghosts"].researched = true

	return force.technologies["issy-enable-ghosts"].researched
end

function enable_ghosts(player)
	if player.force.technologies["issy-enable-ghosts"].researched then
		player.print({"issy-ghost-enabler.already-enabled"})
	else
		if enable_ghosts_force(player.force) then
			player.force.print({"issy-ghost-enabler.enabled", player.name})
		else
			player.print({"issy-ghost-enabler.failed"})
		end
	end
end

script.on_event({
	defines.events.on_force_created,
	defines.events.on_force_reset
}, function(event)
	if not enable_ghosts_force(event.force) then
		log(string.format("failed to enable ghosts for force %s", event.force.name))
	end
end)

function on_init()
	for name, force in pairs(game.forces) do
		if not (name == "enemy" or name == "neutral") then
			if not enable_ghosts_force(force) then
				log(string.format("failed to enable ghosts for force %s", name))
			end
		end
	end

	on_load()
end

function on_load()
	commands.add_command("enable-ghosts", {"issy-ghost-enabler.description"}, function(stuff)
		enable_ghosts(game.players[stuff.player_index])
	end)
end

script.on_init(on_init)
script.on_load(on_load)
