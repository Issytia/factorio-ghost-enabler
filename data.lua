data:extend({
	{
		type = "technology",
		name = "issy-enable-ghosts",
		icon_size = data.raw.technology["construction-robotics"].icon_size,
		icon_mipmaps = data.raw.technology["construction-robotics"].icon_mipmaps,
		icon = data.raw.technology["construction-robotics"].icon,
		hidden = true,
		effects = {
			{
				type = "create-ghost-on-entity-death",
				modifier = true
			},
			{
				type = "deconstruction-time-to-live",
				modifier = 60 * 60 * 60 * 8 * 365
			}
		},
		unit = {
			count = 1,
			ingredients = {
				{ "automation-science-pack", 1 }
			},
			time = 1
		}
	}
})
